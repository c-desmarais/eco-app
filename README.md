# eco-app

## Development

### Prerequisites

Install [Unity Hub](https://unity3d.com/get-unity/download) and make sure you are running Unity Version `2019.4.0.f1 LTS`.

Install [Git LFS](https://git-lfs.github.com/), which allows large files handling for Git.

### Data persistence

For now, data is saved in the filesystem.

Under windows, you may access the saved data in : `.../AppData/LocalLow/DefaultCompany/eco-app/Data`

## Manual Debugging in Editor

- Press `S` to change Skybox color