﻿using UnityEngine;

public class OursAnimationController : Singleton<OursAnimationController>
{
    public Animator animator;

    private EcosystemEventManager ecosystemEventManager;

    public float randomMinTimeTransition = 10;
    public float randomMaxTimeTransition = 30;

    private float delay = 0;

    [SerializeField] private ParticleSystem LoveParticles;
    [SerializeField] private ParticleSystem FoodParticles;

    void Start()
    {
        // find event manager
        ecosystemEventManager = GameObject.FindObjectOfType<EcosystemEventManager>();
        if (ecosystemEventManager != null)
        {
            // Bind animator to scene events
            ecosystemEventManager.OnModelChanged.AddListener(ToggleOnUserInteractionTrigger);
        }

        delay = mGenerateRandomDelay();
    }

    private void ToggleOnUserInteractionTrigger()
    {
        if (!animator.GetBool("user_interaction"))
        {
            animator.SetTrigger("user_interaction");
        }
    }

    public void ToggleEatTrigger()
    {
        animator.SetTrigger("eat");
        FoodParticles.Play();
    }

    public void ToggleAffectionTrigger()
    {
        Debug.Log("toggleAffectionTrigger");
        int randomNb = Random.Range(0, 100);
        if (randomNb % 2 == 0)
        {
            animator.SetTrigger("affection_1");
        } else
        {
            animator.SetTrigger("affection_2");
        }
        LoveParticles.Play();
    }

    void Update()
    {
        if (delay <= 0)
        {
            // Launch the transition
            animator.SetTrigger("random_occurence");

            // Restart the next delay
            delay = mGenerateRandomDelay();
            Debug.Log("delay " + delay.ToString());
        }
        else
        {
            delay = delay - Time.deltaTime;
        }
    }

    private float mGenerateRandomDelay()
    {
        return Random.Range(randomMinTimeTransition, randomMaxTimeTransition);
    }
}
