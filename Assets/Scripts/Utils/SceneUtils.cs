﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

static public class SceneUtils
{
    static public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
