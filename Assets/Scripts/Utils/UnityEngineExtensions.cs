﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityEngine
{
    public static class UnityEngineExtensions
    {
        public static T[] GetComponentsInDirectChildren<T>(this Component parent) where T : Component
        {
            return parent.GetComponentsInDirectChildren<T>(false);
        }

        public static T[] GetComponentsInDirectChildren<T>(this Component parent, bool includeInactive) where T : Component
        {
            List<T> tmpList = new List<T>();
            foreach (Transform transform in parent.transform)
            {
                if (includeInactive || transform.gameObject.activeInHierarchy)
                {
                    tmpList.AddRange(transform.GetComponents<T>());
                }
            }
            return tmpList.ToArray();
        }

        public static T GetFirstDisabledComponentInGrandChildren<T>(this Component parent) where T : Component
        {
            foreach (Transform transform in parent.transform)
            {
                foreach (Transform subtransform in transform)
                {
                    if (subtransform.gameObject.activeSelf == false)
                    {
                        return subtransform.GetComponent<T>();
                    }
                }
            }

            return null;
        }

        public static T[] GetComponentsInGrandChildren<T>(this Component parent, bool includeInactive) where T : Component
        {
            List<T> tmpList = new List<T>();
            foreach (Transform transform in parent.transform)
            {
                foreach (Transform subtransform in transform.transform)
                {
                    if (includeInactive || subtransform.gameObject.activeInHierarchy)
                    {
                        tmpList.AddRange(subtransform.GetComponents<T>());
                    }
                }
            }
            return tmpList.ToArray();
        }
    }
}
