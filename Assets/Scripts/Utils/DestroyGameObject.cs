﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyGameObject : MonoBehaviour
{
    public void Run(GameObject gameObject)
    {
        Destroy(gameObject);
    }
}
