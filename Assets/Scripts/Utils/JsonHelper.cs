﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

using UnityEngine;

public static class JsonHelper
{
    public static T deserializeJSON<T>(string str)
    {
        return JsonConvert.DeserializeObject<T>(str);
    }

    public static string serializeJSON<T>(T obj)
    {
        return JsonConvert.SerializeObject(obj);
    }
}