using UnityEngine;

public static class GameStateManager
{
    static string NavigationScene;

    static public void SetNavigationScene(string navigationScene)
    {
        NavigationScene = navigationScene;
    }

    static public string GetNavigationScene()
    {
        return NavigationScene;
    }
}
