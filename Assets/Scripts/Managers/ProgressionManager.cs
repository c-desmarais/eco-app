﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelProgressionStats
{
    public int AccumulatedXpPoints;
    public int RequiredXpPoints;
    public int Level;
    public LevelProgressionStats(
        int numeratorPoints = 0,
        int denominatorPoints = 0,
        int currentLevel = 0)
    {
        AccumulatedXpPoints = numeratorPoints;
        RequiredXpPoints = denominatorPoints;
        Level = currentLevel;
    }
}

class LevelProgressionDataItem
{
    public int MaxLevel;
    public Dictionary<string, int> Conditions;
    public LevelProgressionDataItem(int maxLevel, Dictionary<string, int> conditions)
    {
        MaxLevel = maxLevel;
        Conditions = conditions;
    }
}

class Progression
{
    public int XpPoints;
    public int CurrencyPoints;

    // a key value dictionnary indicating (id of challenge completed: how many times that challenge has been completed)
    public Dictionary<string, int> AccomplishedChallenges;

    public List<string> EcoItemIds;

    public List<DateTime> StrikeList;

    public DateTime? LastTimePlayed;

    public bool IsTutorialComplete;

    public Progression(
        DateTime? lastTimePlayed = null,
        int xpPoints = 0,
        int currencyPoints = 0, 
        Dictionary<string, int> accomplishedChallenges = null,
        List<string> ecoItemIds = null, 
        List<DateTime> strikeList = null,
        bool isTutorialComplete = false)
    {
        CurrencyPoints = currencyPoints;
        XpPoints = xpPoints;
        AccomplishedChallenges = accomplishedChallenges ?? new Dictionary<string, int>();
        EcoItemIds = ecoItemIds ?? new List<string>();
        StrikeList = strikeList ?? new List<DateTime>();
        LastTimePlayed = lastTimePlayed;
        IsTutorialComplete = isTutorialComplete;
    }
}


public static class ProgressionManager
{
    private static string SCORE_FILE_PATH = Application.persistentDataPath + "/Data/player-progression.json";
    private static int MAX_DAYS_STRIKE = 7;
    private static int POINTS_PER_STRIKE = 3000;

    private static Progression Progression;
    private static LevelProgressionStats LevelProgressionStats = new LevelProgressionStats();

    private static List<LevelProgressionDataItem> LevelProgressionData;

    public static void initialize()
    {
        Progression = loadProgressionFromFile();
        LevelProgressionData = loadLevelProgressionDataFromFile();
        addDailyLogin();
        updateLevelProgressionStats();
    }

    public static int getScore()
    {
        return Progression.CurrencyPoints;
    }

    public static int GetXpPoints()
    {
        return Progression.XpPoints;
    }

    public static int getPointsPerStrike()
    {
        return POINTS_PER_STRIKE;
    }

    public static void addPointsForStrike()
    {
        Progression.CurrencyPoints += POINTS_PER_STRIKE;
    }

    public static void setEcoItemIds(List<string> ecoItemIds)
    {
        Progression.EcoItemIds = ecoItemIds;
    }

    public static List<string> getEcoItemIds()
    {
        return Progression.EcoItemIds;
    }

    public static bool GetIsTutorialComplete()
    {
        return Progression.IsTutorialComplete;
    }

    public static void SetIsTutorialComplete(bool isTutorialComplete)
    {
        Progression.IsTutorialComplete = isTutorialComplete;
    }

    public static DateTime? getLastTimePlayed()
    {
        return Progression.LastTimePlayed;
    }

    public static void SpendCurrencyPoints(int currencySpent)
    {
        if (Progression.CurrencyPoints - currencySpent >= 0)
        {
            Progression.CurrencyPoints = Progression.CurrencyPoints - currencySpent;
        } else
        {
            throw new InvalidOperationException();
        }
    }

    public static void progress(Challenge challenge)
    {
        Progression.XpPoints += int.Parse(challenge.XpPoints);
        Progression.CurrencyPoints += int.Parse(challenge.CurrencyPoints);

        // accomplish a challenge
        if (Progression.AccomplishedChallenges.ContainsKey(challenge.Id))
        {
            int timesAccomplished = Progression.AccomplishedChallenges[challenge.Id];
            Progression.AccomplishedChallenges[challenge.Id] = ++timesAccomplished;
        }
        else
        {
            Progression.AccomplishedChallenges.Add(challenge.Id, 1);
        }

        updateLevelProgressionStats();
    }

    public static void updateLevelProgressionStats()
    {
        if(LevelProgressionData != null && LevelProgressionData.Count > 0)
        {
            int idx = 0;

            int levelNb = 1;
            int remainingScore = Progression.CurrencyPoints;

            int xpRequired = LevelProgressionData[idx].Conditions["xp"];
            int maxLevel = LevelProgressionData[idx].MaxLevel;

            bool ultimateLevelReached = false;

            while (remainingScore - xpRequired >= 0 && !ultimateLevelReached)
            {
 
                levelNb++;
                
                if (levelNb == maxLevel)
                {
                    idx += 1;
                    ultimateLevelReached = idx == LevelProgressionData.Count;
                }

                if (!ultimateLevelReached)
                {
                    remainingScore -= xpRequired;
                    maxLevel = LevelProgressionData[idx].MaxLevel;
                    xpRequired = LevelProgressionData[idx].Conditions["xp"];
                } else
                {
                    remainingScore = -1;
                    xpRequired = -1;
                }
            }

            SetProgressionLevelStats(levelNb, remainingScore, xpRequired);
        }
    }

    public static LevelProgressionStats GetProgressionLevelStats()
    {
        return LevelProgressionStats;
    }

    private static void SetProgressionLevelStats(int level, int numerator, int denominator)
    {
        LevelProgressionStats.Level = level;
        LevelProgressionStats.AccumulatedXpPoints = numerator;
        LevelProgressionStats.RequiredXpPoints = denominator;
    }

    public static bool isStrike()
    {
        return Progression.StrikeList.Count == MAX_DAYS_STRIKE;
    }

    public static void clearStrike()
    {
        Progression.StrikeList.Clear();
    }

    public static void addDailyLogin()
    {
        DateTime now = DateTime.UtcNow;

        //TODO : now.Date returns utc time (which can be in the future for instance if you are in mtl)
        Boolean isConsecutive = Progression.StrikeList.Count > 0 && Progression.StrikeList[Progression.StrikeList.Count - 1].AddDays(1).Date == now.Date;
        Boolean alreadyLoggedInToday = Progression.StrikeList.Exists(date => now.Date == date.Date);

        if (Progression.StrikeList.Count == 0 || (isConsecutive && Progression.StrikeList.Count + 1 < MAX_DAYS_STRIKE))
        {
            // If array is empty or if date is consecutive without exceeding strike nb
            Progression.StrikeList.Add(now);
        }
        else if (isConsecutive && Progression.StrikeList.Count + 1 == MAX_DAYS_STRIKE)
        {
            Progression.StrikeList.Add(now);
            // If you have made a strike
            //HabitsSceneUI.Instance.showStrikeHabitView();
            //Progression.StrikeList.Clear();
        }
        else if (!alreadyLoggedInToday) // we don't clear when user logs multiple times on the same day
        {
            // If it is not consecutive and there is already something in the list.
            Progression.StrikeList.Clear();
            Progression.StrikeList.Add(now);
        }
    }

    public static List<DateTime> getConsecutiveLogins()
    {
        return Progression.StrikeList;
    }

    private static Progression loadProgressionFromFile()
    {
        string jsonString = "";

        Progression progression = null;
        if (File.Exists(SCORE_FILE_PATH))
        {
            jsonString = File.ReadAllText(SCORE_FILE_PATH);
            progression = JsonHelper.deserializeJSON<Progression>(jsonString);
        } 
        
        if (progression == null) // if the file existed but was corrupt it will deserialize to null and fall in this case
        {
            progression = new Progression();
        }

        return progression;
    }

    private static List<LevelProgressionDataItem> loadLevelProgressionDataFromFile()
    {
        // Resources folder will persist in the build (contrary to application data path)
        TextAsset file = Resources.Load<TextAsset>("Data/level-progression");
        string content = file.text;
        List<LevelProgressionDataItem> levelProgressionData = JsonHelper.deserializeJSON<List<LevelProgressionDataItem>>(content);
        return levelProgressionData;
    }

    public static void saveProgression()
    {
        Debug.Log("SavingProgression");
        Progression.LastTimePlayed = DateTime.Now;
        string progressionStr = JsonHelper.serializeJSON(Progression);
        Directory.CreateDirectory(Application.persistentDataPath + "/Data"); // Does not create it if it already exists
        File.WriteAllText(SCORE_FILE_PATH, progressionStr);
    }
}
