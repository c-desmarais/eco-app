﻿using UnityEngine;
using System.IO;

class PlayerInfo
{
    public string Name;
    public string TargetNbTimesPerWeek;

    public PlayerInfo(string name = null, string targetNbTimesPerWeek = null)
    {
        this.Name = name;
        this.TargetNbTimesPerWeek = targetNbTimesPerWeek;
    }
}

public static class PlayerInfoManager
{
    private static PlayerInfo PlayerInfo;

    public static void initialize()
    {
        PlayerInfo = loadPlayerInfoFromFile(Application.persistentDataPath + "/Data/player-info.json");
    }

    public static string getPlayerName()
    {
        return PlayerInfo.Name;
    }

    public static string getPlayerTargetNbTimesPerWeek()
    {
        return PlayerInfo.TargetNbTimesPerWeek;
    }

    public static void setPlayerInfo(string name, string targetNbTimesPerWeek)
    {
        PlayerInfo.Name = name;
        PlayerInfo.TargetNbTimesPerWeek = targetNbTimesPerWeek;
    }

    // TODO : extract this logic outside (since its used by many managers)
    public static void savePlayerInfoToFile()
    {
        string dirPath = Application.persistentDataPath + "/Data";

        string PlayerInfoStr = JsonHelper.serializeJSON(PlayerInfo);

        Directory.CreateDirectory(dirPath); // Does not create it if it already exists
        File.WriteAllText(dirPath + "/player-info.json", PlayerInfoStr);
    }

    private static PlayerInfo loadPlayerInfoFromFile(string path)
    {
        string jsonString = "";

        PlayerInfo playerInfo = null;

        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path);
            playerInfo = JsonHelper.deserializeJSON<PlayerInfo>(jsonString);
        }

        if (playerInfo == null)
        {
            playerInfo = new PlayerInfo(); // if the file existed and is corrupt it will resovle to null and fallback here
        }

        return playerInfo;
    }
}
