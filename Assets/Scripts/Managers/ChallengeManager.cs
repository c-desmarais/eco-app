﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class ChallengeManager
{
    private static List<Challenge> favoriteChallenges;
    private static List<Challenge> dailyChallenges;

    private static int MAX_FAVES = 6;
    private static int MAX_SUGGESTED_CHALLENGES = 6;

    public static void initialize()
    {
        List<Challenge> possibleChallenges = loadChallengeCatalog();
        dailyChallenges = selectRandomChallenges(possibleChallenges);

        favoriteChallenges = loadChallengesFromFile(Application.persistentDataPath + "/Data/faved-challenges.json");
    }

    public static List<Challenge> getDailyChallenges()
    {
        return dailyChallenges;
    }

    public static List<Challenge> getFavoriteChallenges()
    {
        return favoriteChallenges;
    }

    public static int getMaxFaves()
    {
        return MAX_FAVES;
    }

    public static bool removeFavorite(Challenge challenge)
    {
        return favoriteChallenges.Remove(challenge);
    }

    public static bool addFavorite(Challenge challenge)
    {
        if (favoriteChallenges.Count < MAX_FAVES)
        {
            favoriteChallenges.Add(challenge);

            return true;
        } else
        {
            Debug.Log("You have reached you're max amounts of favorites.");
            return false;
        };
    }

    public static bool isInFavorites(Challenge challenge)
    {
        int idx = favoriteChallenges.FindIndex(chal => chal.Equals(challenge));
        return idx >= 0;
    }

    public static void saveFavoritesToFile()
    {
        string dirPath = Application.persistentDataPath + "/Data";

        string favoritesChallengesStr = JsonHelper.serializeJSON(favoriteChallenges);

        Directory.CreateDirectory(dirPath); // Does not create it if it already exists
        File.WriteAllText(dirPath + "/faved-challenges.json", favoritesChallengesStr);
    }

    private static List<Challenge> loadChallengesFromFile(string path)
    {
        string jsonString = "";

        List<Challenge> challenges = null;

        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path);
            challenges = JsonHelper.deserializeJSON<List<Challenge>>(jsonString);
        } 

        if (challenges == null)
        {
            challenges = new List<Challenge>(); // if the file existed and is corrupt it will resovle to null and fallback here
        }

        return challenges;
    }

    private static List<Challenge> loadChallengeCatalog()
    {
        // Resources folder will persist in the build (contrary to application data path)
        TextAsset file = Resources.Load<TextAsset>("Data/challenges");
        string content = file.text;
        List<Challenge> challenges = JsonHelper.deserializeJSON<List<Challenge>>(content);
        return challenges;
    }

    private static List<Challenge> selectRandomChallenges(List<Challenge> potentialChallenges)
    {
        int nbChallengesToSelect = MAX_SUGGESTED_CHALLENGES;

        List<Challenge> shuffledChallenges = new List<Challenge>(potentialChallenges);

        // shuffle Challenges with Fisher-Yates algorithm
        for (int i = shuffledChallenges.Count - 1; i > 0; i--)
        {
            // pick a random index from 0 to i
            int j = UnityEngine.Random.Range(0, i + 1);

            Challenge temp = shuffledChallenges[i];
            shuffledChallenges[i] = shuffledChallenges[j];
            shuffledChallenges[j] = temp;
        }

        // Pick max non-conflicting challenges
        List<Challenge> pickedChallenges = new List<Challenge>();
        List<string> excludedChallengesIds = new List<string>();

        for (int i = 0; i < shuffledChallenges.Count && pickedChallenges.Count < nbChallengesToSelect; i++)
        {
            Challenge currentChallenge = shuffledChallenges[i];
            if (!excludedChallengesIds.Contains(currentChallenge.Id))
            {
                for (int j = 0; j < currentChallenge.ExcludedChallengeIds.Count; j++)
                {
                    excludedChallengesIds.Add(currentChallenge.ExcludedChallengeIds[j]);
                }

                pickedChallenges.Add(currentChallenge);
            }
        }

        return pickedChallenges;
    }
}
