﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    void Awake()
    {
        PlayerInfoManager.initialize();
        ProgressionManager.initialize();
        ChallengeManager.initialize();

        DontDestroyOnLoad(this);
        GameStateManager.SetNavigationScene("Ecosystem");
        SceneManager.LoadScene(1);
    }

    private void OnApplicationPause(bool isPaused)
    {
        Debug.Log("On application pause: " + isPaused);
        if (isPaused)
        {
            ProgressionManager.saveProgression();
            ChallengeManager.saveFavoritesToFile();
        }
    }

    // On application quit is not guaranteed to be called on android devices
    private void OnApplicationQuit()
    {
        Debug.Log("On application quit");
        ProgressionManager.saveProgression();
        ChallengeManager.saveFavoritesToFile();
        PlayerInfoManager.savePlayerInfoToFile();
    }
}
