using UnityEngine;
using TMPro;

public class ProgressionSlider : MonoBehaviour
{
    [SerializeField] private GameObject OuterSlider;
    [SerializeField] private GameObject InnerSlider;
    [SerializeField] private GameObject PointsCircle;
    [SerializeField] private TextMeshProUGUI AccPoints;
    [SerializeField] private TextMeshProUGUI DenomPoints;

    public void UpdatePlayerStats()
    {
        LevelProgressionStats stats = ProgressionManager.GetProgressionLevelStats();
        string accPointsText = stats.AccumulatedXpPoints.ToString();
        AccPoints.SetText(accPointsText);

        string denomPointsText = stats.RequiredXpPoints.ToString();
        DenomPoints.SetText("/ " + denomPointsText);

        RectTransform rectTransformOuter = (RectTransform)OuterSlider.transform;
        RectTransform rectTransformInner = (RectTransform)InnerSlider.transform;

        float width = (stats.AccumulatedXpPoints * rectTransformOuter.rect.width) / stats.RequiredXpPoints;

        // Setting width of inner slider
        InnerSlider.GetComponent<RectTransform>().sizeDelta = new Vector2(width, rectTransformOuter.sizeDelta.y);

        Vector2 shiftDirection = new Vector2(0.5f - rectTransformInner.anchorMax.x, 0.5f - rectTransformOuter.anchorMax.y);
        rectTransformInner.anchoredPosition = shiftDirection * rectTransformInner.rect.size;
    }
}
