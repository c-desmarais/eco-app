﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class Model3DUtils
{
    // Model3D COMPARISON METHODS
    static public int CompareByAscendingUnlockLevel(Model3D modelA, Model3D modelB)
    {
        if (modelA == null || modelB == null)
            return 0;

        if (modelA.UnlockLevel == modelB.UnlockLevel)
            return 0; // They are equals
        else if (modelA.UnlockLevel > modelB.UnlockLevel)
            return 1; // Model A is greater
        else
            return -1; // Model B is greater
    }
}
