﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene3DManager : Singleton<Scene3DManager>
{
    private bool saveRequested = false;
    
    void Start()
    {
        Load3DScene();
    }

    private void LateUpdate()
    {
        if (saveRequested)
        {
            saveRequested = false;
            Save3DScene();
        }
    }

    public void RequestSave()
    {
        saveRequested = true;
    }

    private void Load3DScene()
    {
        Debug.Log("Load3DScene");
        // Populate 3D scene with the right models (previous saved scene)
        List<string> modelsIDs = ProgressionManager.getEcoItemIds();

        Debug.Log("modelIds:" + modelsIDs);
        Model3DAnchor[] model3DAnchors = gameObject.GetComponentsInChildren<Model3DAnchor>();

        Debug.Log("how many anchors:" + model3DAnchors.Length);
        for (int i = 0; i < model3DAnchors.Length; i++)
        {
            bool foundID = false;
            foreach(string modelID in modelsIDs)
            {
                if (modelID.Substring(0, 2).Equals(model3DAnchors[i].CategoryID))
                {
                    foundID = true;
                    model3DAnchors[i].LoadModel(modelID);
                    break;
                }
            }
            if (foundID == false)
            {
                // Fallback on the default model
                model3DAnchors[i].LoadModel(null);
            }
        }
    }

    private void Save3DScene()
    {
        // Save the IDs of the current selection made in the scene
        Model3D[] models = gameObject.GetComponentsInChildren<Model3D>();
        string[] modelsIDs = new string[models.Length];
        for (int i = 0; i < models.Length; i++)
        {
            modelsIDs[i] = models[i].ModelID;
        }
        // Update values and save them
        ProgressionManager.setEcoItemIds(new List<string>(modelsIDs));
        Debug.Log("Scene3DManager -> Save3DScene");
        ProgressionManager.saveProgression();
    }
}
