﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FloorQuadrant { First, Second, Third, Fourth };
public enum FloorTheme { Winter, Forest };

public class FloorManager : Singleton<FloorManager>
{
    [SerializeReference]
    private string resourcesPath = "EcosystemModels/Floors";

    [SerializeReference]
    private FloorTheme selectedTheme = FloorTheme.Forest;

    // Currently instantiated floor objects
    //Dictionary<string, FloorPart> instantiatedFloors = new Dictionary<string, FloorPart>();
    [SerializeReference] List<FloorPart> instantiatedFloors = new List<FloorPart>();

    Dictionary<string, List<FloorPart>> floorDictionary;

    public void ChangeFloorTheme(FloorTheme theme)
    {
        if (theme != selectedTheme)
        {
            selectedTheme = theme;
            // Update Floors based on the present models in the scene
            Model3D[] modelsInTheScene = FindObjectsOfType<Model3D>();
            foreach (Model3D model in modelsInTheScene)
            {
                if (IsFloorRequired(model.GetCategoryID()))
                    InstantiateFloor(model);
            }
        }
    }
    public void ChangeFloorTheme(int theme)
    {
        ChangeFloorTheme((FloorTheme)theme);
    }

    public bool IsFloorRequired(string categoryID)
    {
        List<string> lKeys = new List<string>(GetFloorDictionary().Keys);
        Debug.Log("IsFloorRequired lkeys" + lKeys);
        return lKeys.Exists(categoryID.Equals);
    }

    public void InstantiateFloor(Model3D model)
    {
        Debug.Log("Instantiatefloor for model" + model.name);
        string cat = model.GetCategoryID();

        // Find the floor to instantiate
        FloorPart floorToInstantiate = null;

        // First check if the model has a specialized floor
        FloorOverride floorOverrideFound = model.GetComponent<FloorOverride>();

        if (floorOverrideFound != null)
        {
            FloorPart floorOverride = floorOverrideFound.GetFloorPart(selectedTheme);
            if (floorOverride != null)
            {
                floorToInstantiate = floorOverride;
            }
            else
            {
                floorToInstantiate = FindSelectedThemeFloor(cat);
            }
        }
        else
        {
            floorToInstantiate = FindSelectedThemeFloor(cat);
        }

        InstantiateFloor(floorToInstantiate);
    }

    private void InstantiateFloor(FloorPart floorToInstantiate)
    {
        // Delete previous floor
        Debug.Log("private instantitate floor");
        DeleteFloor(floorToInstantiate);

        // Instantiate new floor
        if (floorToInstantiate != null)
        {
            FloorPart instantiatedFloor = Instantiate(floorToInstantiate, this.transform);
            instantiatedFloors.Add(instantiatedFloor);
            //instantiatedFloors.Add(floorToInstantiate.CategoryID, instantiatedFloor);
        }
    }

    private FloorPart FindSelectedThemeFloor(string category)
    {
        Dictionary<string, List<FloorPart>> floorDict = GetFloorDictionary();
        if (floorDict.ContainsKey(category))
        {
            floorDict.TryGetValue(category, out List<FloorPart> floorsList);
            return FindSelectedThemeFloor(floorsList);
        }
        return null;
    }

    private FloorPart FindSelectedThemeFloor(List<FloorPart> floorsList)
    {
        foreach (FloorPart floor in floorsList)
        {
            if (floor.Theme == selectedTheme)
                return floor;
        }
        return null;
    }

    private List<FloorPart> LoadFloorPrefabsFromResources()
    {
        UnityEngine.Object[] floorPrefabs = Resources.LoadAll(resourcesPath);
        List<FloorPart> floors = new List<FloorPart>();
        foreach (GameObject prefab in floorPrefabs)
        {
            FloorPart[] floorPartComponentFound = prefab.GetComponents<FloorPart>();
            if (floorPartComponentFound.Length > 0)
            {
                floors.Add(floorPartComponentFound[0]);
            }
        }
        return floors;
    }

    private Dictionary<string, List<FloorPart>> CreateFloorDictionary(List<FloorPart> floorParts)
    {
        Dictionary<string, List<FloorPart>> modelsCatalogue = new Dictionary<string, List<FloorPart>>();
        foreach (FloorPart floor in floorParts)
        {
            string categoryID = floor.CategoryID;
            if (modelsCatalogue.ContainsKey(categoryID))
            {
                modelsCatalogue.TryGetValue(categoryID, out List<FloorPart> modelsList);
                modelsList.Add(floor);
            }
            else
            {
                List<FloorPart> newList = new List<FloorPart>();
                newList.Add(floor);
                modelsCatalogue.Add(categoryID, newList);
            }
        }

        return modelsCatalogue;
    }

    private Dictionary<string, List<FloorPart>> GetFloorDictionary()
    {
        if (floorDictionary == null)
        {
            List<FloorPart> floorParts = LoadFloorPrefabsFromResources();
            floorDictionary = CreateFloorDictionary(floorParts);
        }

        return floorDictionary;
    }

    private void DeleteFloor(FloorPart floorpart)
    {
        Debug.Log("delete floor" + floorpart.CategoryID);

        FloorPart floorToDelete = instantiatedFloors.Find(x => x.CategoryID == floorpart.CategoryID);
        if(floorToDelete != null)
        {
            Debug.Log("floor is not null");
            instantiatedFloors.Remove(floorToDelete);
            Destroy(floorToDelete.gameObject);
        }
    }
}
