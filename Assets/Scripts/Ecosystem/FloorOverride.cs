﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorOverride : MonoBehaviour
{
    public FloorPart winterFloor;
    public FloorPart forestFloor;

    public void Awake()
    {
        // Set the category on the floor part
        string catID = GetModelCategoryID();
        if (winterFloor != null)
            winterFloor.CategoryID = catID;
        if (forestFloor != null)
            forestFloor.CategoryID = catID;
    }

    private string GetModelCategoryID()
    {
        Model3D[] modelsPart = GetComponents<Model3D>();
        if (modelsPart.Length == 1)
        {
            return modelsPart[0].GetCategoryID();
        }
        else
        {
            Debug.LogError("Wrong amount of Model3D script component on this game object. gameobject: " + gameObject.ToString() + " . Make sure the gameobject has only one Model3D script component and that the model ID is well defined.");
        }
        Debug.LogError("Couldn't retrieve category ID from the model for floor override: " + gameObject.ToString() + " . Make sure the gameobject also has a Model3D script component and that the model ID is well defined.");
        return "";
    }

    public FloorPart GetFloorPart(FloorTheme floorTheme)
    {
        switch (floorTheme)
        {
            case FloorTheme.Forest:
                return forestFloor;
            default:
                return winterFloor;
        }
    }
}
