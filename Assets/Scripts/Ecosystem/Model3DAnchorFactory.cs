﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Model3DAnchorFactory : MonoBehaviour
{
    [SerializeReference]
    private string resourcesPath = "EcosystemModels/";

    [SerializeReference]
    private GameObject model3DAnchorPrefab = null;

    // Create all Model3DAnchor at awake (before start since other components might need it at start)
    void Awake()
    {
        BuildModel3DAnchors();
    }

    void BuildModel3DAnchors()
    {
        if (model3DAnchorPrefab != null)
        {
            List<Model3D> models = LoadModel3DPrefabsFromResources();
            // Dictionary key = category ID, value = all Models in that category
            Dictionary<string, List<Model3D>> modelsCatalog = CreateCatalog(models);

            // Create the Model3DAnchors from the catalogue
            foreach (KeyValuePair<string, List<Model3D>> entry in modelsCatalog)
            {
                // Instantiate Model3DAnchor prefab
                GameObject newAnchor = Instantiate(model3DAnchorPrefab, this.transform);
                Model3DAnchor model3DAnchorRef = newAnchor.GetComponent<Model3DAnchor>();

                if (model3DAnchorRef != null)
                {
                    model3DAnchorRef.CategoryID = entry.Key;

                    // Sort the models based on their unlocking level // TODO this needs to be confirmed
                    entry.Value.Sort(Model3DUtils.CompareByAscendingUnlockLevel);

                    foreach (Model3D model in entry.Value)
                    {
                        // Add the model to the anchor
                        model3DAnchorRef.Models.Add(model);
                        // Link the anchor in the model
                        model.Anchor = model3DAnchorRef;
                    }
                }
            }
        }
    }

    Dictionary<string, List<Model3D>> CreateCatalog(List<Model3D> models)
    {
        Dictionary<string, List<Model3D>> modelsCatalogue = new Dictionary<string, List<Model3D>>();
        foreach (Model3D model in models)
        {
            string categoryID = model.ModelID.Substring(0, 2);
            if (modelsCatalogue.ContainsKey(categoryID))
            {
                modelsCatalogue.TryGetValue(categoryID, out List<Model3D> modelsList);
                modelsList.Add(model);
            }
            else
            {
                List<Model3D> newList = new List<Model3D>();
                newList.Add(model);
                modelsCatalogue.Add(categoryID, newList);
            }
        }

        return modelsCatalogue;
    }

    List<Model3D> LoadModel3DPrefabsFromResources()
    {
        UnityEngine.Object[] resources = Resources.LoadAll(resourcesPath);
        GameObject[] prefabs = Resources.FindObjectsOfTypeAll<GameObject>();
        List<Model3D> models = new List<Model3D>();
        foreach (GameObject prefab in prefabs)
        {
            Model3D[] modelComponentFound = prefab.GetComponents<Model3D>();
            if (modelComponentFound.Length > 0)
            {
                models.Add(modelComponentFound[0]);
            }
        }
        return models;
    }
}
