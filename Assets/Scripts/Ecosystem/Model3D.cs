﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model3D : MonoBehaviour
{
    [SerializeReference]
    private string modelName = "";
    public string ModelName { get => modelName; }

    [SerializeReference]
    private Sprite modelIcon = null;
    public Sprite ModelIcon { get => modelIcon; }

    [SerializeReference]
    [Tooltip("Must be unique. Please refere to the model id spreadsheet on drive.")]
    private string modelID = null;
    public string ModelID { get => modelID; }

    [SerializeReference]
    private int unlockLevel = 0;
    public int UnlockLevel { get => unlockLevel; }

    private Model3DAnchor anchor;
    public Model3DAnchor Anchor { get => anchor; set => anchor = value; }

    public string GetCategoryID()
    {
        if (ModelID == null || ModelID.Equals("") || ModelID.Length!=4)
            Debug.LogError("Wrong ModelID for gameobject: " + gameObject.ToString() + " . Make sure the gameobject's ModelID is well defined.");

        return ModelID?.Substring(0, 2);
    }
}
