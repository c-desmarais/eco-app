﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// This class is responsible for holding events related to the ecosystem scene.
// OnModelChanged 
// OnNewAssetAvailable
// OnThemeChanged
// OnBackgroundChanged
public class EcosystemEventManager : MonoBehaviour
{
    public UnityEvent OnModelChanged;
    // OnNewAssetAvailable
    // OnThemeChanged
    // OnBackgroundChanged
}
