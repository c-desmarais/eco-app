﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Process simple touch to make object selection
public class SelectionManager : MonoBehaviour
{
    public GameObject modelSelectionPopup;

    public GameObject fitContent;

    // Start is called before the first frame update
    void Start()
    {
        SceneInputManager.Instance.getSimpleTouchEvt().AddListener(OnSimpleTouch);
    }

    private void OnSimpleTouch()
    {
        // Find if a Model3D was selected. If so, it gets swap for another.
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Transform hitTransform = hit.transform;
            Model3D selectedModel3D = hitTransform.GetComponent<Model3D>();
            if (selectedModel3D != null && selectedModel3D.Anchor.Models.Count > 1)
            {
                CreateSelectionPopup(selectedModel3D);
            }
        }
    }

    private void CreateSelectionPopup(Model3D selectedModel3D)
    {
        // Return if there is already a popup in the scene
        if (fitContent.GetComponentsInChildren<ChooserPanel>().Length > 0) return;

        if (modelSelectionPopup != null)
        {
            GameObject panel = Instantiate(modelSelectionPopup) as GameObject;
            ChooserPanel chooserPanel = panel.GetComponent<ChooserPanel>();
            chooserPanel.model3DAnchor = selectedModel3D.Anchor;
            if (fitContent != null)
            {
                Transform canvasTransfo = fitContent.GetComponent<Transform>();
                Transform panelTransfo = panel.GetComponent<Transform>();
                if (canvasTransfo != null && chooserPanel != null)
                {
                    panelTransfo.SetParent(canvasTransfo, false);
                }
            }
        }
    }
}
