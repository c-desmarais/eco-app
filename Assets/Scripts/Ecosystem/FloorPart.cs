﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorPart : MonoBehaviour
{
    [SerializeReference]
    [Tooltip("Corresponds to model ID first two digits. Please refere to the model id spreadsheet on drive.")]
    private string categoryID = null;
    public string CategoryID { get => categoryID; set => categoryID = value; }

    [SerializeReference]
    private FloorTheme theme = FloorTheme.Winter;
    public FloorTheme Theme { get => theme; }
}
