﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

// This class manages simple click events in the 3D scene. 
public class SceneInputManager : Singleton<SceneInputManager>
{
    private Vector2 startPos;
    private float startTime;

    UnityEvent simpleTouchEvt;

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        startPos = touch.position;
                        startTime = Time.time;
                    }
                    break;
                    
                case TouchPhase.Ended:
                    float deltaTime = Time.time - startTime;
                    if (deltaTime <= 0.25f)
                    {
                        //Fire a simple touch event 
                        getSimpleTouchEvt().Invoke();
                    }
                    break;
            }
        }

#if UNITY_EDITOR
        // mouse click for testing purposes
        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
                getSimpleTouchEvt().Invoke();
        }
#endif

    }

    public UnityEvent getSimpleTouchEvt()
    {
        if (simpleTouchEvt == null)
            simpleTouchEvt = new UnityEvent();
        return simpleTouchEvt;
    }
}
