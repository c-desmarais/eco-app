﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model3DAnchor : MonoBehaviour
{
    private Model3D currentModel;

    [SerializeReference]
    private string categoryID = null;
    public string CategoryID { get => categoryID; set => categoryID = value; }

    [SerializeReference]
    private List<Model3D> models = new List<Model3D>();

    public List<Model3D> Models { get => models; set => models = value; }

    EcosystemEventManager ecosystemEventManager;
    void Start()
    {
        // find event manager
        ecosystemEventManager = GameObject.FindObjectOfType<EcosystemEventManager>();
    }

    // Loads the specified model ID if it exists, if not, it loads this anchor's first model.
    // This method must be called during the 3D scene loading only.
    public void LoadModel(string modelID)
    {
        // Try to find the right model to instantiate it
        foreach (Model3D model in Models)
        {
            if (model.ModelID.Equals(modelID))
            {
                InstantiateModel(model);
                return;
            }
        }
        //Fallback load this anchor first model
        if (Models.Count >= 1)
        {
            InstantiateModel(Models[0]);
        }
    }

    public void ChangeModel(Model3D newModel)
    {
        bool success = false;
        if (newModel != null)
        {
            if (currentModel == null)
            {
                InstantiateModel(newModel);
                success = true;
            }
            else if (newModel.ModelID != currentModel.ModelID)
            {
                Destroy(currentModel.gameObject);
                InstantiateModel(newModel);
                success = true;
            }

            if (success)
            {
                Scene3DManager.Instance.RequestSave();
                if (ecosystemEventManager != null)
                {
                    ecosystemEventManager.OnModelChanged.Invoke();
                }
            }
        }
    }

    private void InstantiateModel(Model3D model)
    {
        currentModel = Instantiate(model,this.transform);
        currentModel.Anchor = this;

        // Add corresponding floor when needed
        if (FloorManager.Instance.IsFloorRequired(model.GetCategoryID()))
        {
            FloorManager.Instance.InstantiateFloor(model);
        }
    }
}
