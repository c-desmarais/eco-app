﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDatas : MonoBehaviour
{
    public static string playerName;
    public static string lastConnexion;
    public static bool notification;
    
    void Start()
    {
        if (PlayerPrefs.HasKey("playerName"))
        {
            playerName = PlayerPrefs.GetString("playerName");
        }
        else
        {
            playerName = "";
        }

        if (PlayerPrefs.HasKey("lastConnexion"))
        {
            lastConnexion = PlayerPrefs.GetString("lastConnexion");
        }
        else
        {
            lastConnexion = System.DateTime.Now.ToString();
        }

        if (PlayerPrefs.HasKey("notification"))
        {
            notification = bool.Parse(PlayerPrefs.GetString("notification"));
        }
        else
        {
            notification = true;
        }
    }

    void OnDisable()
    {
        PlayerPrefs.SetString("name", playerName);

        lastConnexion = System.DateTime.Now.ToString();
        PlayerPrefs.SetString("lastConnexion", lastConnexion);

        PlayerPrefs.SetString("notification", notification.ToString());
    }
}
