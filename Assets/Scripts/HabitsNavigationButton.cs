using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Michsky.UI.ModernUIPack;

public class HabitsNavigationButton : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip clip;

    public void Select()
    {
        ButtonManagerBasic btnManager = gameObject.GetComponent<ButtonManagerBasic>();
        btnManager.normalText.color = new Color(1f, 1f, 1f, 1f); // white

        UIGradient btnGradient = gameObject.GetComponent<UIGradient>();
        btnGradient.enabled = true;
        
        btnManager.UpdateUI();
    }

    public void Unselect()
    {
        ButtonManagerBasic btnManager = gameObject.GetComponent<ButtonManagerBasic>();
        btnManager.normalText.color = new Color32(255, 127, 130, 255); // pink

        UIGradient btnGradient = gameObject.GetComponent<UIGradient>();
        btnGradient.enabled = false;

        btnManager.UpdateUI();
    }
}
