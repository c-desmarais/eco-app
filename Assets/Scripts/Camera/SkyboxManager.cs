using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class SkyboxManager : MonoBehaviour
{
    public List<Material> SkyboxMaterials;
    private Skybox CamSkybox;
    int SkyboxCounter = 0;

    void Start()
    {
        CamSkybox = GetComponent<Skybox>();
        CamSkybox.material = SkyboxMaterials[SkyboxCounter];
    }

    public void ChangeSkyBox(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            int nbMaterials = SkyboxMaterials.Count;

            SkyboxCounter = (SkyboxCounter + 1) % nbMaterials;
            CamSkybox.material = SkyboxMaterials[SkyboxCounter];
        }
    }
}
