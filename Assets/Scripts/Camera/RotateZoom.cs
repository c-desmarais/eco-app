﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// Attach this script to a camera that is placed under an empty game object (pivot).
public class RotateZoom : MonoBehaviour
{
    [SerializeReference]
    private GameObject pivot = null;

    [SerializeReference]
    private float zoomIntensity = 0.1f;

    [SerializeReference]
    private float zoomIntensityMouse = 10f;

    [SerializeReference]
    private float minZoom = -15f;

    [SerializeReference]
    private float maxZoom = -3f;

    [SerializeReference]
    private float rotationIntensity = 0.01f;

    [SerializeReference]
    private float minPitch = 0f;

    [SerializeReference]
    private float maxPitch = 60f;

    [SerializeReference]
    private float maxYaw = 50f;

    private Vector3 touchStartPos;

    // Update is called once per frame
    void Update()
    {
        //SKIP if the pointer is over UI
        if (Input.GetMouseButtonDown(0) && !(IsMouseOverUI() || IsFingerOverUI()))
        {
            touchStartPos = Input.mousePosition;
        }

        if (Input.touchCount == 2 && !IsFingerOverUI())
        {
            // Get the two fingers Touch info
            Touch firstFinger = Input.GetTouch(0);
            Touch secondFinger = Input.GetTouch(1);

            Vector3 firstFingerPrevPos = firstFinger.position - firstFinger.deltaPosition;
            Vector3 secondFingerPrevPos = secondFinger.position - secondFinger.deltaPosition;

            float prevMagnitude = (firstFingerPrevPos - secondFingerPrevPos).magnitude;
            float currentMagnitude = (firstFinger.position - secondFinger.position).magnitude;

            float deltaMagnitude = currentMagnitude - prevMagnitude;

            Zoom(deltaMagnitude * zoomIntensity);
        }
        else if (Input.GetMouseButton(0) && !(IsMouseOverUI() || IsFingerOverUI()))
        {
            Vector3 deltaPos = touchStartPos - Input.mousePosition;
            deltaPos.x *= -1;
            Rotate(deltaPos);
        }

        // Zoom using the mouse wheel (for testing purposes)
        if (!IsMouseOverUI())
        {
            Zoom(Input.GetAxis("Mouse ScrollWheel") * zoomIntensityMouse);
        }
    }

    private static bool IsMouseOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    // Returns true if at least one finger is over UI.
    private static bool IsFingerOverUI()
    {
        bool atLeastOneFingerOnUI = false;
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                atLeastOneFingerOnUI |= EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId);
            }
        }
        return atLeastOneFingerOnUI;
    }

    private void Zoom(float delta)
    {
        Vector3 camPosInfo = Camera.main.transform.position;
        if (((camPosInfo.z > minZoom) && delta < 0f) || ((camPosInfo.z < maxZoom) && delta > 0f))
        {
            Vector3 zoomDir = (pivot.transform.position - camPosInfo).normalized;
            Camera.main.transform.position += delta * zoomDir;
        }
    }

    private void Rotate(Vector3 deltaPos)
    {
        Quaternion angle = Quaternion.Euler(deltaPos.y, deltaPos.x, 0);

        float pitch = pivot.transform.rotation.eulerAngles.x;
        float yaw = pivot.transform.rotation.eulerAngles.y;

        if (pitch > 180)
        {
            pitch -= 360;
        }

        if ((deltaPos.y < 0 && pitch > minPitch) || (deltaPos.y > 0 && pitch < maxPitch))
        {
            pivot.transform.Rotate(new Vector3(1, 0, 0), deltaPos.y * rotationIntensity);
        }

        if (yaw > 180)
        {
            yaw -= 360;
        }

        if ((deltaPos.x < 0 && yaw > -maxYaw) || (deltaPos.x > 0 && yaw < maxYaw))
        {
            pivot.transform.Rotate(new Vector3(0, 1, 0), deltaPos.x * rotationIntensity, Space.World);
        }
    }
}
