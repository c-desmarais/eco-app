﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Settings : MonoBehaviour
{
    public UnityEvent OnNotificationPrefChange;

    readonly string NOTIFICATION_KEY = "NotificationEnabled";
    bool isNotifEnabled = false;
    public bool IsNotifEnabled { get => isNotifEnabled; set => SetIsNotifEnabled(value); }

    readonly string NOTIFICATION_NEW_CHALLENGES_KEY = "NewChallengesNotificationEnabled";
    bool isDailyNotifEnabled = false;
    public bool IsDailyNotifEnabled { get => isDailyNotifEnabled; set => SetIsDailyNotifEnabled(value); }

    readonly string NOTIFICATION_CHALLENGE_GOAL_KEY = "ChallengeGoalNotificationEnabled";
    bool isRetentionNotifEnabled = false;
    public bool IsRetentionNotifEnabled { get => isRetentionNotifEnabled; set => SetIsRetentionNotifEnabled(value); }

    public void Awake()
    {
        isNotifEnabled = GetPlayerPref(NOTIFICATION_KEY);
        isDailyNotifEnabled = GetPlayerPref(NOTIFICATION_NEW_CHALLENGES_KEY);
        isRetentionNotifEnabled = GetPlayerPref(NOTIFICATION_CHALLENGE_GOAL_KEY);
    }

    /// <summary>
    /// Gets the value from the player preference file for the specified key if it exists.
    /// </summary>
    /// <param name="key"></param>
    /// <returns> the player pref, or false if not found </returns>
    private bool GetPlayerPref(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            string lastSavedPrefs = PlayerPrefs.GetString(key);
            if (lastSavedPrefs != null)
                return bool.Parse(lastSavedPrefs);
        }
        return false;
    }



    private void SetIsNotifEnabled(bool onOff)
    {
        if (IsNotifEnabled != onOff)
        {
            isNotifEnabled = onOff;
            SetPreference(NOTIFICATION_KEY, onOff);
        }
    }

    private void SetIsDailyNotifEnabled(bool onOff)
    {
        if (IsDailyNotifEnabled != onOff)
        {
            isDailyNotifEnabled = onOff;
            SetPreference(NOTIFICATION_NEW_CHALLENGES_KEY, onOff);
        }
    }

    private void SetIsRetentionNotifEnabled(bool onOff)
    {
        if (IsRetentionNotifEnabled != onOff)
        {
            isRetentionNotifEnabled = onOff;
            SetPreference(NOTIFICATION_CHALLENGE_GOAL_KEY, onOff);
        }
    }

    private void SetPreference(string playerPrefKey, bool onOff)
    {
            UpdatePlayerPrefsFile(playerPrefKey, onOff);
            // trigger event change
            OnNotificationPrefChange.Invoke();
    }

    /// <summary>
    /// Saves the new value in the preference file.
    /// </summary>
    /// <param name="newValue"></param>
    /// <param name="playerPrefKey"></param>
    private static void UpdatePlayerPrefsFile(string playerPrefKey, bool newValue)
    {
        PlayerPrefs.SetString(playerPrefKey, newValue.ToString());
        PlayerPrefs.Save();
    }
}
