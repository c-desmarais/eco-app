﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Notifications.Android;
using System;

public class NotificationManager : MonoBehaviour
{
    private readonly string CHANNEL_ID = "heko-notif-channel";
    private readonly string DAILY_CHANNEL_ID = "heko-notif-daily-channel";
    AndroidNotificationChannel mainChannel;
    AndroidNotificationChannel dailyChannel;
    Settings settings;

    public int DailyNotifHour = 7;

    public void Awake()
    {
        // Start with a clean state
        AndroidNotificationCenter.CancelAllNotifications();

        // Setup channels
        mainChannel = new AndroidNotificationChannel(CHANNEL_ID, "HEKO_NOTIF", "TEST", Importance.Default);
        AndroidNotificationCenter.RegisterNotificationChannel(mainChannel);

        dailyChannel = new AndroidNotificationChannel(DAILY_CHANNEL_ID, "HEKO_DAILY_NOTIF", "De nouveaux défis sont disponibles.", Importance.Default);
        AndroidNotificationCenter.RegisterNotificationChannel(dailyChannel);
        
        // Find settings
        settings = GameObject.FindObjectOfType<Settings>();

        // Register to notification settings change
        RegisterToNotificationSettingsChange();
    }

    private void RegisterToNotificationSettingsChange()
    {
        if (settings != null)
        {
            settings.OnNotificationPrefChange.AddListener(() => {
                ScheduleNotifications();
            });
        }
    }

    public void Start()
    {
        ScheduleNotifications();
    }

    private void ScheduleNotifications()
    {
        // Remove previous notifications
        AndroidNotificationCenter.CancelAllNotifications();

        if (settings != null && settings.IsNotifEnabled) {
            if (settings.IsDailyNotifEnabled)
                ScheduleDailyNotif();
            if (settings.IsRetentionNotifEnabled)
                ScheduleRetentionNotif();
        }
    }

    private void ScheduleRetentionNotif()
    {
        // Find when to send a reminder
        double nbChallengesPerWeek = 7.0;
        if (PlayerInfoManager.getPlayerTargetNbTimesPerWeek() != null)
        {
            if (double.TryParse(PlayerInfoManager.getPlayerTargetNbTimesPerWeek(), out double parsedPlayerTarget))
                nbChallengesPerWeek = parsedPlayerTarget;
        }

        double delay = Math.Max(1.0, Math.Floor(7.0 / nbChallengesPerWeek));

        DateTime reminderDate = DateTime.Now.AddDays(delay);

        ScheduleNotification(CHANNEL_ID, //
            CreateNotification("C'est l'heure de faire un défi écolo!", //
            "Complète des défis pour atteindre " + nbChallengesPerWeek + " défis/sem! ", //
            reminderDate));
    }

    private void ScheduleDailyNotif()
    {
        int year = DateTime.Now.Year;
        int day = DateTime.Now.AddDays(1.0).Day;
        int month = DateTime.Now.Month;
        ScheduleNotification(DAILY_CHANNEL_ID, //
            CreateNotification("Nouveaux défis écolos!", //
            "De nouveaux défis écologiques t'attendent! ", //
            new DateTime(year, month, day, DailyNotifHour, 0, 0), TimeSpan.FromDays(1)));
    }

    public void DoNotification() // Method to test notifications
    {
        // Send a notification in 5 seconds
        ScheduleNotification(CHANNEL_ID, //
            CreateNotification("C'est l'heure de faire un défi écolo!", //
            "Il te reste Y jours afin de compléter X défis! ", //
            DateTime.Now.AddSeconds(5)));
    }

    public void ScheduleNotification(string channel_id, AndroidNotification notification)
    {
        if (settings != null && settings.IsNotifEnabled)
        {
            AndroidNotificationCenter.SendNotification(notification, channel_id);
        }
    }

    private AndroidNotification CreateNotification(string title, string text, DateTime dateTime)
    {
        AndroidNotification notification = new AndroidNotification();
        notification.Title = title;
        notification.Text = text;
        notification.FireTime = dateTime;
        return notification;
    }

    private AndroidNotification CreateNotification(string title, string text, DateTime dateTime, TimeSpan repeatTime)
    {
        AndroidNotification notification = CreateNotification(title,text,dateTime);
        notification.RepeatInterval = TimeSpan.FromDays(1);
        return notification;
    }
}
