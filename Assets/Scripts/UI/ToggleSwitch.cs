using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSwitch : MonoBehaviour
{
    public Animator animator;

    private bool Enabled = true;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        if (Enabled)
        {
            Enabled = false;
            animator.Play("toggle_off");
        } else
        {
            Enabled = true;
            animator.Play("toggle_on");
        }
    }
}
