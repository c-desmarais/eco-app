﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpandingOptionsMenu : MonoBehaviour
{
    public List<Button> buttons = new List<Button>();

    public Button currentSelection;

    public GameObject panel;

    private void Start()
    {
        int index = GetInitialSelection();
        if (buttons[index] != currentSelection)
        {
            buttons[index].onClick.Invoke();
        }
    }

    protected virtual int GetInitialSelection()
    {
        return 0;
    }

    public void OnButtonClicked(Button selection)
    {
        if (currentSelection != null && selection == currentSelection)
        {
            // Toggle menu visibility (open or close)
            panel.SetActive(!panel.activeSelf);
        }
        else
        {
            UpdateSelection(selection);
        }
    }

    private void UpdateSelection(Button selection)
    {
        // Update selection
        SwapButtonsPosition(selection);
        currentSelection = selection;
        SaveSelection(buttons.FindIndex(selection.Equals));
        // Close panel
        panel.SetActive(false);
    }

    // Update the button selection by placing the newly selected button 
    // at the current selection button position, and the previous selection 
    // at the other button position.
    private void SwapButtonsPosition(Button selection)
    {
        Vector3 selectedButtonPosition = currentSelection.transform.position;
        Transform ParentCurrentSelection = currentSelection.transform.parent;

        // Exchange both buttons position
        currentSelection.transform.position = selection.transform.position;
        currentSelection.transform.SetParent(selection.transform.parent);
        selection.transform.position = selectedButtonPosition;
        selection.transform.SetParent(ParentCurrentSelection);
    }

    protected virtual void SaveSelection(int indexSelection)
    {
        // must be implemented by class extending it
    }
}
