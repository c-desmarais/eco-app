﻿using System;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerStatsUI : MonoBehaviour
{
    // Start is called before the first frame update
    public Image[] strikeIcons;

    public TextMeshProUGUI points;

    [SerializeField] private TextMeshProUGUI XpPointsText;

    public TextMeshProUGUI LevelText;

    public ProgressionSlider progressionSlider;

    public static PlayerStatsUI Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Debug.Log("Destroyed Instance id : " + gameObject.GetInstanceID());
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdatePlayerStats();

        if (ProgressionManager.isStrike())
        {
            HabitsSceneUI.Instance.showStrikeHabitView();
        }
    }

    public void UpdateScoreInformation()
    {
        CultureInfo cultureInfo = new CultureInfo("fr-CA");

        string pts = string.Format("{0:#,0}", ProgressionManager.getScore());
        points.SetText(pts);

        string xpPoints = string.Format("{0:#,0}", ProgressionManager.GetXpPoints());
        XpPointsText.SetText(xpPoints);

        LevelProgressionStats lvlProgressionStats = ProgressionManager.GetProgressionLevelStats();

        string levelPrefix = "Niv. ";
        string levelSuffix = lvlProgressionStats.Level.ToString();

        if (levelSuffix.Length == 1)
        {
            levelSuffix = "0" + levelSuffix;
        }

        LevelText.SetText(levelPrefix + levelSuffix);

        progressionSlider.UpdatePlayerStats();
    }

    public void UpdateStrike()
    {
        List<DateTime> strikeList = ProgressionManager.getConsecutiveLogins();

        int dayNb = (int)strikeList[0].DayOfWeek;

        for (int i = 0; i < strikeIcons.Length; i++)
        {
            string dayLetter = getDayLetter(dayNb);
            strikeIcons[i].GetComponentInChildren<TextMeshProUGUI>().SetText(dayLetter);

            Image innerImage = strikeIcons[i].GetComponentsInDirectChildren<Image>()[0];

            if (i <= strikeList.Count - 1)
            {
                // completed pink
                innerImage.color = new Color32(254, 90, 91, 230);
            }
            else
            {
                // white orange (white orange, not completed)
                innerImage.color = new Color32(254, 154, 0, 163);
            }

            dayNb = (dayNb + 1) % strikeIcons.Length;
        }
    }

    private string getDayLetter(int dayNb)
    {
        string dayLetter = "";
        switch(dayNb)
        {
            case 0:
                dayLetter = "D";
                break;
            case 1:
                dayLetter = "L";
                break;
            case 2:
                dayLetter = "M";
                break;
            case 3:
                dayLetter = "M";
                break;
            case 4:
                dayLetter = "J";
                break;
            case 5:
                dayLetter = "V";
                break;
            case 6:
                dayLetter = "S";
                break;
            default:
                break;
        }

        return dayLetter;
    }

    public void UpdatePlayerStats()
    {
        UpdateStrike();
        UpdateScoreInformation();
    }
}
