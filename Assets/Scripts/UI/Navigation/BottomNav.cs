using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomNav: MonoBehaviour
{
    public List<NavigationButton> buttons;
    public AudioClip clip;

    public void OnNavigationClick(string sceneName)
    {
        SoundManager.Instance.PlaySound(clip);

        // reset buttons
        for(int i = 0; i < buttons.Count; i++)
        {
            buttons[i].Reset();
        }

        // Set Navigation Scene
        GameStateManager.SetNavigationScene(sceneName);

        // load scene
        SceneUtils.LoadScene(sceneName);
    }
}
