﻿using UnityEngine;
using UnityEngine.EventSystems;// Required when using Event data.
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        if (PlayerInfoManager.getPlayerName() != null)
        {
            SceneManager.LoadScene(3);
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }
}
