﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavigationButton : MonoBehaviour
{
    public Sprite unclickedImage;
    public Sprite clickedImage;
    public Image buttonIcon;
    public string sceneName;

    private void Start()
    {
        if (GameStateManager.GetNavigationScene() == sceneName)
        {
            buttonIcon.sprite = clickedImage;
        } else
        {
            buttonIcon.sprite = unclickedImage;
        }
    }

    public void Reset()
    {
        buttonIcon.sprite = unclickedImage;
    }
}
