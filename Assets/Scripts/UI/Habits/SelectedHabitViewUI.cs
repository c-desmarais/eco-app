﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectedHabitViewUI : MonoBehaviour
{
    // UI
    [SerializeField] private TextMeshProUGUI currencyPointsText;
    [SerializeField] private TextMeshProUGUI xpPointsText;
    public TextMeshProUGUI summary;
    public TextMeshProUGUI description;
    public Button goButton;
    public Button backButton;
    public FaveBtn faveButton;
    public AudioClip GoButtonAudioClip;
    public AudioClip BackButtonAudioClip;

    // Challenge
    private Challenge selectedChallenge;

    private void Start()
    {
        goButton.onClick.AddListener(onGoButtonClick);
        backButton.onClick.AddListener(onBackButtonClick);
    }

    public void SetSelectedChallengeUI(Challenge challenge)
    {
        currencyPointsText.SetText(challenge.CurrencyPoints);
        xpPointsText.SetText(challenge.XpPoints);
        summary.SetText(challenge.Summary);
        description.SetText(challenge.Description);

        if (ChallengeManager.isInFavorites(challenge))
        {
            faveButton.setFave(true);
        } else
        {
            faveButton.setFave(false);
        };

        faveButton.updateFaveImage();

        selectedChallenge = challenge;
    }

    public void onBackButtonClick()
    {
        SoundManager.Instance.PlaySound(BackButtonAudioClip);
        HabitsSceneUI.Instance.showHabitsView();
    }

    public void onGoButtonClick ()
    {
        SoundManager.Instance.PlaySound(GoButtonAudioClip);
        ProgressionManager.progress(selectedChallenge);
        PlayerStatsUI.Instance.UpdateScoreInformation();
        HabitsSceneUI.Instance.showHabitsView();
    }

    public Challenge getSelectedChallenge()
    {
        return selectedChallenge;
    }
}
