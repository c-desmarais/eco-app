﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;

public class HabitsViewUI : MonoBehaviour
{
    public Button favesButton;
    public Button habitsButton;

    public GameObject favesButtonPanel;
    public GameObject habitsButtonPanel;

    private ChallengeButton[] favesButtons;

    public static HabitsViewUI Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Debug.Log("Destroyed Instance id : " + gameObject.GetInstanceID());
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        favesButtons = favesButtonPanel.transform.GetComponentsInGrandChildren<ChallengeButton>(true);
        favesButton.onClick.AddListener(() => OnClick(favesButton));
        habitsButton.onClick.AddListener(() => OnClick(habitsButton));

        InitializeChallengesButtons();
        ResetFavoriteButtons();

        showHabits();
    }

    private void OnClick(Button button)
    {
        // Play sound
        HabitsNavigationButton habitsNavButton = button.GetComponent<HabitsNavigationButton>();
        AudioClip audioClip = habitsNavButton.clip;
        SoundManager.Instance.PlaySound(audioClip);

        if (button.name == "FavoritesButton")
        {
            showFaves();
        } else
        {
            showHabits();
        }
    }


    private void InitializeChallengesButtons()
    {
        List<Challenge> chosenChallenges = ChallengeManager.getDailyChallenges();
        
        ChallengeButton[] challengeButtons = habitsButtonPanel.transform.GetComponentsInDirectChildren<ChallengeButton>();

        for (int i = 0; i < challengeButtons.Length; i++)
        {
            Challenge currentChallenge = chosenChallenges[i];

            challengeButtons[i].Setup(currentChallenge);
        }
    }

    private void ResetFavoriteButtons()
    {
        List<Challenge> favoriteChallenges = ChallengeManager.getFavoriteChallenges();

        for (int i = 0; i < favesButtons.Length; i++)
        {            
            if (i < favoriteChallenges.Count)
            {
                Challenge currentChallenge = favoriteChallenges[i];
                assignFavoriteButton(currentChallenge, i);
            } else
            {
                favesButtons[i].gameObject.SetActive(false);
            }
        }
    }

    public void showFaves()
    {
        favesButton.GetComponent<HabitsNavigationButton>().Select();
        habitsButton.GetComponent<HabitsNavigationButton>().Unselect();
        favesButtonPanel.SetActive(true);
        habitsButtonPanel.SetActive(false);
    }

    public void showHabits()
    {
        habitsButton.GetComponent<HabitsNavigationButton>().Select();
        favesButton.GetComponent<HabitsNavigationButton>().Unselect();
        favesButtonPanel.SetActive(false);
        habitsButtonPanel.SetActive(true);
    }

    private void assignFavoriteButton(Challenge challenge, int index)
    {

        ChallengeButton faveChallengeButton = favesButtons[index];
        faveChallengeButton.Setup(challenge);
        faveChallengeButton.gameObject.SetActive(true);
        faveChallengeButton.SetFave(true);



        // for a given favorite, find the corresponding habit button if there is one and fave it
        ChallengeButton regularChallengeButton = Array.Find(habitsButtonPanel.GetComponentsInChildren<ChallengeButton>(), item =>
        {
            return item.GetChallenge().Equals(challenge);
        });

        if (regularChallengeButton)
        {
            regularChallengeButton.SetFave(true);
        }
    }

    public void removeFavoriteChallenge(Challenge challenge)
    {
        // Remove favorite in challenge manager
        if (!ChallengeManager.removeFavorite(challenge)) return;

        ChallengeButton regularButton = findChallengeButton(challenge, habitsButtonPanel);
        ChallengeButton favedButton = findChallengeButton(challenge, favesButtonPanel);

        // Unfave regular button
        // It is possible that the regular button does not exist anymore due to a refresh
        if (regularButton)
        {
            regularButton.SetFave(false);
        } 

        if (favedButton)
        {
            //favedButton.GetComponent<ChallengeButton>().setFave(false);
            favedButton.SetFave(false);
        }

        // Reorder buttons
        ResetFavoriteButtons();
    }

    public void addFavoriteChallenge(Challenge challenge)
    {
        if (!ChallengeManager.addFavorite(challenge)) return;

        assignFavoriteButton(challenge, ChallengeManager.getFavoriteChallenges().Count - 1);
    }

    private ChallengeButton findChallengeButton(Challenge challenge, GameObject panel)
    {
        return Array.Find(panel.GetComponentsInChildren<ChallengeButton>(), item =>
        {
            return item.GetChallenge().Equals(challenge);
        });
    }
}
