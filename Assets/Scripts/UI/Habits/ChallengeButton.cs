﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChallengeButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI PointsText;
    [SerializeField] private TextMeshProUGUI SummaryText;

    [SerializeField] private Image CategoryImage;
    [SerializeField] private Image FaveImage;
    [SerializeField] private Image BackgroundImage;

    [SerializeField] private AudioClip Clip;

    private Challenge Challenge;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void Setup(Challenge challenge)
    {
        Challenge = challenge;
        SummaryText.SetText(Challenge.Summary);
        PointsText.SetText(Challenge.CurrencyPoints);
        CategoryImage.sprite = Resources.Load<Sprite>("Images/Habits/" + Challenge.Category);
        BackgroundImage.sprite = Resources.Load<Sprite>("Images/Habits/" + Challenge.Category + "_bg");
    }

    private void OnClick()
    {
        SoundManager.Instance.PlaySound(Clip);
        HabitsSceneUI.Instance.showSelectedHabitView(Challenge);
    }

    public Challenge GetChallenge()
    {
        return Challenge;
    }

    public void SetFave(bool isFave)
    {
        FaveImage.gameObject.SetActive(isFave);
    }
}
