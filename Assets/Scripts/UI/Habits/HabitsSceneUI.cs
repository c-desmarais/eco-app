﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HabitsSceneUI : MonoBehaviour
{
    public GameObject selectedHabitCanvas;
    public GameObject habitsCanvas;
    public GameObject strikeCanvas;
    public Animator uiAnimator;

    public static HabitsSceneUI Instance { get; private set; }

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
            // We do not use dont destroy on load here, because we want only ONE habitsSceneUI (singleton)
            // for a single scene (challenges scene). We dont need this singleton to be present across multiple sceens.
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        uiAnimator = GetComponent<Animator>();
    }

    public void showSelectedHabitView(Challenge selectedChallenge)
    {
        SelectedHabitViewUI hp = selectedHabitCanvas.GetComponent<SelectedHabitViewUI>();
        hp.SetSelectedChallengeUI(selectedChallenge);
        uiAnimator.SetTrigger("selected_habit_open");
    }

    public void showStrikeHabitView()
    {
        StrikeViewUI svui = strikeCanvas.GetComponent<StrikeViewUI>();
        svui.SetStrikeUIContent();
        uiAnimator.SetTrigger("strike_open");
    }

    public void showHabitsView()
    {
        if (selectedHabitCanvas.activeSelf)
        {
            uiAnimator.SetTrigger("selected_habit_close");
        }

        if (strikeCanvas.activeSelf)
        {
            uiAnimator.SetTrigger("strike_close");
        }
    }
}
