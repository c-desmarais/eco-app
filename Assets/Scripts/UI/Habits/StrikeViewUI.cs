﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StrikeViewUI : MonoBehaviour
{
    public Button goButton;

    public TextMeshProUGUI pointsText;
    public TextMeshProUGUI mainText;
    public GameObject strikePopup;

    private int NbPoints;

    private void Start()
    {
        goButton.onClick.AddListener(onGoButtonClick);
    }

    public void SetStrikeUIContent()
    {
        mainText.SetText("Cela fait maintenant " + ProgressionManager.getConsecutiveLogins().Count.ToString() + " jours que tu joues.");
        pointsText.SetText(ProgressionManager.getPointsPerStrike().ToString() + " points");
    }

    public void onGoButtonClick()
    {
        ProgressionManager.addPointsForStrike();
        ProgressionManager.clearStrike();
        PlayerStatsUI.Instance.UpdatePlayerStats();
        HabitsSceneUI.Instance.showHabitsView();
    }
}
