﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaveBtn : MonoBehaviour
{
    public Sprite favedIcon;
    public Sprite unfavedIcon;
    public AudioClip clip;

    private bool isFaved;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        SoundManager.Instance.PlaySound(clip);
        Challenge challenge = this.transform.parent.parent.gameObject.GetComponent<SelectedHabitViewUI>().getSelectedChallenge();
        if (isFaved == false)
        {
            HabitsViewUI.Instance.addFavoriteChallenge(challenge);
            setFave(true);
        }
        else
        {
            HabitsViewUI.Instance.removeFavoriteChallenge(challenge);
            setFave(false);
        }

        updateFaveImage();
    }

    public void updateFaveImage()
    {
        //Button selectedButton = this.transform.parent.parent.gameObject.GetComponent<Button>();
        if (isFaved)
        {
            GetComponent<Image>().sprite = favedIcon;
        } else
        {
            GetComponent<Image>().sprite = unfavedIcon;
        }
    }

    public void setFave(bool fave)
    {
        isFaved = fave;
    }

    public bool getFave()
    {
        return isFaved;
    }

}
