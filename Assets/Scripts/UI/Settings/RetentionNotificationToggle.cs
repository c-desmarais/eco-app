﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetentionNotificationToggle : MonoBehaviour
{
    Settings settings;
    public Toggle toggle;

    private void Awake()
    {
        // Find settings
        settings = GameObject.FindObjectOfType<Settings>();
    }

    void Start()
    {
        if(settings!=null)
            toggle.isOn = settings.IsRetentionNotifEnabled;
    }
}
