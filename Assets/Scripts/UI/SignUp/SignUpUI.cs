﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Michsky.UI.ModernUIPack;
using TMPro;

public class SignUpUI : MonoBehaviour
{
    public Button firstButton;
    public Button secondButton;
    public GameObject firstPanel;
    public GameObject secondPanel;
    
    private TMP_InputField usernameText;
    private TextMeshProUGUI errorText;
    private TextMeshProUGUI nbTimesText;

    // Start is called before the first frame update
    void Start()
    {
        firstButton.onClick.AddListener(onFirstButtonClick);
        secondButton.onClick.AddListener(onSecondButtonClick);
        
        SliderManager sliderManager = secondPanel.GetComponentInChildren<SliderManager>();
        //sliderManager.onValueChanged.AddListener(onSliderUpdate);
        
        usernameText = secondPanel.GetComponentInChildren<TMP_InputField>();
        errorText = secondPanel.GetComponentInChildren<TextMeshProUGUI>();

        nbTimesText = sliderManager.GetComponentInChildren<TextMeshProUGUI>();
    }

    public void onSliderUpdate(float sliderValue)
    {
        if (sliderValue < 7)
        {
            nbTimesText.SetText(sliderValue.ToString());
        }
        else
        {
            nbTimesText.SetText(sliderValue.ToString() + " ou +");
        }
    }

    private void onSecondButtonClick()
    {
        if (string.IsNullOrEmpty(usernameText.text))
        {
            errorText.gameObject.SetActive(true);
        } else
        {
            errorText.gameObject.SetActive(false);
            PlayerInfoManager.setPlayerInfo(usernameText.text, nbTimesText.text);
            SceneManager.LoadScene(3);
        }
    }

    private void onFirstButtonClick()
    {
        firstPanel.SetActive(false);
        secondPanel.SetActive(true);
    }

}
