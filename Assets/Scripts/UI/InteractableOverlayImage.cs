﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Class that shows an overlay image depending on whether a button is interactable or not (disabled).
public class InteractableOverlayImage : MonoBehaviour
{
    [SerializeReference]
    private GameObject overlayImage = null;

    [SerializeReference]
    private Button button = null;

    [SerializeReference]
    private bool showWhenInteractable = false;

    void Update()
    {
        if (overlayImage == null || button == null) return;

        // Checks if the image visibility needs to be updated
        if (showWhenInteractable && (button.interactable != overlayImage.activeSelf))
        {
            overlayImage.SetActive(button.interactable);
        }
        else if (!showWhenInteractable && (button.interactable == overlayImage.activeSelf))
        {
            overlayImage.SetActive(!button.interactable);
        }
    }
}
