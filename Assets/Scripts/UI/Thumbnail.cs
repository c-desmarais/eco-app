﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Thumbnail : MonoBehaviour
{
    public Sprite Image;

    public void SetImage(Sprite NewImage)
    {
        Image = NewImage;

        Image imageRef = gameObject.GetComponent<Image>();
        if (imageRef != null)
        {
            imageRef.sprite = Image;
        }
    }
}
