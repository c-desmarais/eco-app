using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using System;

public class FoodButton : MonoBehaviour
{
    [SerializeField] private Sprite ActiveImage;
    [SerializeField] private Sprite InactiveImage;

    [SerializeField] private AudioClip Clip;

    private int COST_FEED = 1000;

    Button button;

    public void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnFeedClick);
        
        UpdateFeed();   
    }

    public void OnFeedClick()
    {
        SoundManager.Instance.PlaySound(Clip);
        try
        {
            ProgressionManager.SpendCurrencyPoints(COST_FEED);
            UpdateUI();
        }
        catch (InvalidOperationException)
        {
            // do nothing
        }
    }

    private void UpdateUI()
    {
        UpdateFeed();
        PlayerStatsUI.Instance.UpdatePlayerStats();
        OursAnimationController.Instance.ToggleEatTrigger();
    }

    private void UpdateFeed()
    {
        int currentCurrency = ProgressionManager.getScore();
        if (currentCurrency < COST_FEED)
        {
            GetComponent<Image>().sprite = InactiveImage;
            DisableButton();
        } else
        {
            GetComponent<Image>().sprite = ActiveImage;
            EnableButton();
        }
    }

    private void DisableButton()
    {
        button.interactable = false;
    }

    private void EnableButton()
    {
        button.interactable = true;
    }

}
