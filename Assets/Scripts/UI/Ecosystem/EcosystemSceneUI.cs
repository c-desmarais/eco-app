using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;

public class EcosystemSceneUI : MonoBehaviour
{
    // Tutorial
    public GameObject TutorialPanel;
    private static int CurrentStep = 0;
    private static List<string> TutorialDialog = new List<string> { "test1", "test2", "test3", "test4", "test5", "test1", "test2", "test3", "test4", "test5" };
    [SerializeField] private TextMeshProUGUI TutorialText;

    void Start()
    {        
        if (ProgressionManager.GetIsTutorialComplete() == false)
        {
            Debug.Log("EcosystemScene UI - Start if");
            TutorialText.SetText(TutorialDialog[CurrentStep]);
            TutorialPanel.SetActive(true);
        } else {
            Debug.Log("EcosystemScene UI - Start else");
            TutorialPanel.SetActive(false);
        }
    }

    public void OnTutorialClick(InputAction.CallbackContext context)
    {
        if (context.performed && TutorialPanel.activeSelf) // on button release
        {
            if (CurrentStep < TutorialDialog.Count - 1)
            {
                CurrentStep++;
                TutorialText.SetText(TutorialDialog[CurrentStep]);
                Debug.Log("CurrentStep" + CurrentStep);
            } else {
                TutorialPanel.SetActive(false);
                ProgressionManager.SetIsTutorialComplete(true);
                Debug.Log("Deactivating");
                // ProgressionManager, set tutorial complete
            }
        }
    }
}
