using UnityEngine;
using UnityEngine.UI;

public class AffectionButton : MonoBehaviour
{
    [SerializeField] private Sprite ActiveImage;
    [SerializeField] private Sprite InactiveImage;

    [SerializeField] private AudioClip Clip;

    Button button;

    public void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnAffectionClick);
    }

    public void OnAffectionClick()
    {
        Debug.Log("OnAffection Click");
        OursAnimationController.Instance.ToggleAffectionTrigger();
        SoundManager.Instance.PlaySound(Clip);
    }
}
