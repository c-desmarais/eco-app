﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooserPanel : MonoBehaviour
{
    public GameObject buttonPrefab;

    public GameObject content;

    public Model3DAnchor model3DAnchor;

    private List<Button> buttons;

    void Start()
    {
        CreateButtons(model3DAnchor.Models);

        Transform contentTransfo = content.GetComponent<Transform>();
        foreach (Button button in buttons)
        {
            Transform buttonTransfo = button.GetComponent<Transform>();
            if (contentTransfo != null && buttonTransfo != null)
            {
                buttonTransfo.SetParent(contentTransfo, false);
            }
        }

        // Listen to clicks in the 3d scene. Close this panel when a click is made in the scene.
        SceneInputManager.Instance.getSimpleTouchEvt().AddListener(OnSimpleTouchInScene);
    }

    void CreateButtons(List<Model3D> models)
    {
        buttons = new List<Button>();

        foreach (Model3D model in models)
        {
            GameObject button = Instantiate(buttonPrefab) as GameObject;
            Button buttonRef = button.GetComponent<Button>();

            if (buttonRef != null)
            {
                buttons.Add(buttonRef);
                buttonRef.onClick.AddListener(OnModelButtonClick(model));

                // Is the item locked?
                if (ProgressionManager.getScore() < model.UnlockLevel)
                {
                    // Disable the button
                    buttonRef.interactable = false;
                    // Display the required level to unlock
                    Text unlockText = button.GetComponentInChildren<Text>(true);
                    if (unlockText != null)
                    {
                        unlockText.text = model.UnlockLevel.ToString();
                    }
                }

                // Set the icon image
                if (model.ModelIcon != null)
                {
                    Thumbnail thumbnailRef = button.GetComponentInChildren<Thumbnail>();
                    thumbnailRef.SetImage(model.ModelIcon);
                }
            }
        }
    }

    UnityEngine.Events.UnityAction OnModelButtonClick(Model3D model)
    {
        return () => {
            model3DAnchor.ChangeModel(model);
            Destroy(gameObject);
        };
    }

    void OnSimpleTouchInScene()
    {
        Destroy(gameObject);
    }
}
