﻿using System;
using System.Collections.Generic;

public class Challenge
{
    public string Id;
    public string Summary;
    public string CurrencyPoints;
    public string XpPoints;
    public string Description;
    public string IsRepeatable;
    public string Category;
    public string Recurrent;
    public List<string> ExcludedChallengeIds;

    public Challenge(
        string id,
        string summary,
        string currencyPoints,
        string xpPoints,
        string description,
        string isRepeatable,
        string category,
        List<string> excludedChallengeIds,
        string recurrent)
    {
        //TODO : check if we should rewrite this to be an int instead (auto increment) (View hashcode func)
        Id = id;
        Summary = summary;
        CurrencyPoints = currencyPoints;
        XpPoints = xpPoints;
        Description = description;
        IsRepeatable = isRepeatable;
        Category = category;
        ExcludedChallengeIds = excludedChallengeIds;
        Recurrent = recurrent;
    }

    // Inspired by https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1.remove?view=netcore-3.1
    public override bool Equals(object obj)
    {
        Challenge challenge = obj as Challenge;
        if (challenge == null)
        {
            return false;
        } 
        
        return Id.Equals(challenge.Id);
    }

    public override int GetHashCode()
    {
        return Int32.Parse(Id);
    }
}
